package com.example.movieapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;
    @Column(name = "genre")
    private String genre;
    @Column(name = "release_year")
    private String year;
    @Column(name = "director")
    private String director;
    @Column(name = "picture")
    private String picture;
    @Column(name = "trailer")
    private String trailer;


    //Making Relation between characters and movies many to many relation.
    @ManyToMany
    @JoinTable(
            name = "character_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    List<Character> characters;
    @JsonGetter("characters")
    public List<Long> charactersGetter() {
        return characters.stream()
                .map(character -> {
                    return character.getId();
                }).collect(Collectors.toList());
    }

    //column with franchise_id as foreign key
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    Franchise franchise;

    @JsonGetter("franchise")
    public Long franchiseGetter() {
        if(franchise != null){
            return franchise.getId();
        }else{
            return null;
        }
    }
        //Generating the main Constructor


    public Movie(String title, String genre, String year, String director, String picture, String trailer) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.director = director;
        this.picture = picture;
        this.trailer = trailer;
    }
    //Empty Constructor


    public Movie() {
    }

    //Getter and Setter
    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

}







