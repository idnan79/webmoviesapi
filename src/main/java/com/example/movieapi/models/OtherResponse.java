package com.example.movieapi.models;

import org.springframework.http.HttpStatus;

public class OtherResponse {
    public Object data;
    public String message;
    public String error;
    public HttpStatus status;
}
