package com.example.movieapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Franchise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;

    //constructor
    public Franchise(String name, String description) {
        this.name = name;
        this.description = description;
    }

    //empty constructor for the services
    public Franchise() {
    }

    //list of movies of all that has this franchise_id
    @OneToMany
    @JoinColumn(name = "franchise_id")
    List<Movie> movies;

    @JsonGetter("movies")
    public List<Long> moviesGetter() {
        if(movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return movie.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    //needed the ids for services
    public List<Long> moviesIdGetter() {
        if(movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return movie.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    //getters and setters
    public Long getId() {
        return id;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
