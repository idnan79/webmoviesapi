package com.example.movieapi;

import com.example.movieapi.models.Character;
import com.example.movieapi.models.Franchise;
import com.example.movieapi.models.Movie;

import com.example.movieapi.repositories.CharacterRepository;
import com.example.movieapi.repositories.FranchiseRepository;
import com.example.movieapi.repositories.MovieRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
;

    @Component
    public class AppStartRunner implements ApplicationRunner {
        private static final Logger logger = LoggerFactory.getLogger(AppStartRunner.class);

        @Autowired
        CharacterRepository characterRepository;
        @Autowired
        FranchiseRepository franchiseRepository;
        @Autowired
        MovieRepository movieRepository;

        // run method for production
        @Override
        public void run(ApplicationArguments args) throws Exception {
            try {
                if (
                        movieRepository.count() == 0 &&
                                characterRepository.count() == 0 &&
                                franchiseRepository.count() == 0
                ) {
                    ArrayList<Character> charList = new ArrayList<>();
                    Character gollum = new Character("Gollum", "The First", "Smeagol", "male", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ20NGR3zH1Johrc3_3NFkgfGYaTGWFRjaXag&usqp=CAU");
                    Character lightning = new Character("Lightening", "McQueen", "Speedy", "male", "https://upload.wikimedia.org/wikipedia/en/8/82/Lightning_McQueen.png");
                    Character frodo = new Character("Frodo", "Baggins", "Shortie", "male", "https://1.bp.blogspot.com/--nN9E8LB86c/X7Ewn5GEGmI/AAAAAAAAGLc/ueRKDW39iUg_5uBBtUANVOv0jMsBufeIQCLcBGAsYHQ/s1280/Frodo%2BBaggins.png");
                    Character sally = new Character("Sally", "Carrera", "Cutie", "female", "https://static.wikia.nocookie.net/disney/images/2/27/Profile-_Sally_Carrera.png/revision/latest?cb=20190414094900");
                    charList.add(gollum);
                    charList.add(lightning);
                    charList.add(frodo);
                    charList.add(sally);
                    List<Character> repChar = characterRepository.findAll();

                    ArrayList<Movie> movieList = new ArrayList<>();
                    Movie cars1 = new Movie("Cars", "Children", "2006", "John Lasseter", "https://www.imdb.com/title/tt0317219/mediaviewer/rm3794114560/", "https://www.youtube.com/watch?v=SbXIj2T-_uk");
                    Movie cars2 = new Movie("Cars 2", "Children", "2011", "John Lasseter", "https://www.imdb.com/title/tt1216475/mediaviewer/rm1951513344/", "https://www.youtube.com/watch?v=oFTfAdauCOo");
                    Movie lotr = new Movie("Lord of the Rings", "Fantasy", "2001", "Peter Jackson", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976/", "https://www.youtube.com/watch?v=_qVkcLUUrZQ");
                    movieList.add(cars1);
                    movieList.add(cars2);
                    movieList.add(lotr);
                    List<Movie> repMovie = movieRepository.findAll();

                    ArrayList<Franchise> franchisesList = new ArrayList<>();
                    Franchise carsF = new Franchise("Cars", "Cars universe");
                    Franchise lotrF = new Franchise("Lord of the Rings", "Amazing movies");
                    franchisesList.add(carsF);
                    franchisesList.add(lotrF);
                    List<Franchise> repFranch = franchiseRepository.findAll();


                    List<Movie> carsMovies = new ArrayList<>();
                    carsMovies.add(cars1);
                    carsMovies.add(cars2);
                    carsF.setMovies(carsMovies);
                    List<Movie> lotrMovies = new ArrayList<>();
                    lotrMovies.add(lotr);
                    lotrF.setMovies(lotrMovies);

                    List<Character> carsChars = new ArrayList<>();
                    carsChars.add(lightning);
                    carsChars.add(sally);
                    cars1.setCharacters(carsChars);
                    cars2.setCharacters(carsChars);
                    List<Character> lotrChars = new ArrayList<>();
                    lotrChars.add(frodo);
                    lotrChars.add(gollum);
                    lotr.setCharacters(lotrChars);

                    for (Character cx : charList) {
                        characterRepository.save(cx);
                    }
                    for (Movie mx : movieList) {
                        movieRepository.save(mx);
                    }
                    for (Franchise fx : franchisesList) {
                        franchiseRepository.save(fx);
                    }
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }



