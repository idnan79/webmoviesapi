package com.example.movieapi.services;

import com.example.movieapi.models.Character;

import com.example.movieapi.models.Movie;

import com.example.movieapi.repositories.CharacterRepository;
import com.example.movieapi.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CharacterRepository characterRepository;

    //method to show all movies
    public ResponseEntity<List<Movie>> getAllMovies(){
        List<Movie> Movies = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(Movies,status);
    }

    //method to show a specific movie
    public ResponseEntity<Movie> getMovie(@PathVariable Long id){
        Movie returnMovie = new Movie();
        HttpStatus status;
        if(movieRepository.existsById(id)){
            status = HttpStatus.OK;
            returnMovie = movieRepository.findById(id).get();
            returnMovie.charactersGetter();
            returnMovie.franchiseGetter();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie, status);
    }

    //method to add a movie
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        HttpStatus status;
        movie = movieRepository.save(movie);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(movie, status);
    }

    //method to update characters in a movie
    public ResponseEntity<Movie> updateCharacters(@PathVariable Long id, @RequestBody List<Long> characters){
        Movie returnMovie = new Movie();
        HttpStatus status;
        if(movieRepository.existsById(id)){
            status = HttpStatus.OK;
            returnMovie = movieRepository.findById(id).get();
            List<Character> characterList = new ArrayList<>();
            for (Long cId : characters){
                characterList.add(characterRepository.getById(cId));
            }
            returnMovie.setCharacters(characterList);
            returnMovie = movieRepository.save(movieRepository.getById(id));
        } else{
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie, status);
    }

    //method to delete a movie, also updates the movie_character table
    public HttpStatus deleteMovie(@PathVariable Long id){
        Movie returnMovie;
        HttpStatus status;
        if(movieRepository.existsById(id)){
            status = HttpStatus.OK;
            returnMovie = movieRepository.findById(id).get();

            List<Character> characterList = returnMovie.getCharacters();
            List<Movie> movies;
            for (Character c : characterList){
                movies = c.getMovies();
                movies.remove(returnMovie);
                c.setMovies(movies);
            }
            movieRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }


    //method to get all characters in a movie
    public ResponseEntity<List<Long>> getCharacters(@PathVariable Long id){
        Movie returnMovie = new Movie();
        HttpStatus status;
        if(movieRepository.existsById(id)){
            status = HttpStatus.OK;
            returnMovie = movieRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie.charactersGetter(), status);
    }

    // method for update movie
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie){
        Movie returnMovie = new Movie();
        HttpStatus status;
        if(!id.equals(movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie,status);
        }
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }
}
