package com.example.movieapi.services;


import com.example.movieapi.models.Character;
import com.example.movieapi.models.Movie;
import com.example.movieapi.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Service
public class CharacterService {
    @Autowired
    CharacterRepository characterRepository;
    //method to show all characters
    public ResponseEntity<List<Character>> getAllCharacters(){
        List<Character> characters = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters,status);

    }

    //method to show a specific character
    public ResponseEntity<Character> getCharacter(@PathVariable Long id){
        Character returnCharacter = new Character();
        HttpStatus status;
        if(characterRepository.existsById(id)){
            status = HttpStatus.OK;
            returnCharacter = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacter, status);
    }

    //method to add a character
    public ResponseEntity<Character> addCharacter(@RequestBody Character character){
        Character returnCharacter = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnCharacter, status);
    }

    //method to update a character
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character){
        Character returnCharacter = new Character();
        HttpStatus status;
        if(!id.equals(character.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter,status);
        }
        returnCharacter = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnCharacter, status);
    }

    //method to delete a character, also deletes in the character_movie table
    public HttpStatus deleteCharacter(@PathVariable Long id){
        Character returnCharacter;
        HttpStatus status;
        if(characterRepository.existsById(id)){
            status = HttpStatus.OK;
            returnCharacter = characterRepository.findById(id).get();
            List<Movie> movieList = returnCharacter.getMovies();
            List<Character> characters;
            for (Movie m : movieList){
                characters = m.getCharacters();
                characters.remove(returnCharacter);
                m.setCharacters(characters);
            }
            characterRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }
}
