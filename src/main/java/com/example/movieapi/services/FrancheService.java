package com.example.movieapi.services;

import com.example.movieapi.models.Franchise;
import com.example.movieapi.models.Movie;
import com.example.movieapi.repositories.FranchiseRepository;
import com.example.movieapi.repositories.MovieRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Service
public class FrancheService {

    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;

    //method to show all franchises
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises,status);
    }

    //method to show a specific franchise
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

    //method to add a franchise
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        HttpStatus status;
        franchise = franchiseRepository.save(franchise);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(franchise, status);
    }

    //method to update movies in a franchise
    public ResponseEntity<Franchise> updateMovies(@PathVariable Long id, @RequestBody List<Long> moviesId){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
            List<Movie> moviesList = new ArrayList<>();
            for (Long mId : moviesId){
                moviesList.add(movieRepository.getById(mId));
            }
            returnFranchise.setMovies(moviesList);
            returnFranchise = franchiseRepository.save(franchiseRepository.getById(id));
        } else{
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

    //method to delete a franchise, also sets null in franchise-column in movies with the franchise
    public HttpStatus deleteFranchise(@PathVariable Long id){
        Franchise returnFranchise;
        HttpStatus status;
        Movie movie;
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
            List<Long> moviesId = returnFranchise.moviesIdGetter();
            System.out.println(moviesId);
            for (Long i: moviesId){
                movie = movieRepository.findById(i).get();
                movie.setFranchise(null);
            }
            franchiseRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return status;
    }

    //method to get all movies in a franchise
    public ResponseEntity<List<Long>> getMovies(@PathVariable Long id){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise.moviesGetter(), status);
    }

    //method to get all characters in a franchise
    public ResponseEntity<List<Long>> getCharacters(@PathVariable Long id){
        Franchise returnFranchise;
        Movie returnMovie;
        List<Long> characterList = new ArrayList<>();
        HttpStatus status;
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
            List<Long> moviesId = returnFranchise.moviesIdGetter();
            for (Long i: moviesId){
                returnMovie = movieRepository.findById(i).get();
                List<Long> movieChars = returnMovie.charactersGetter();
                for (Long l : movieChars){
                    if (!characterList.contains(l)){
                        characterList.add(l);
                    }
                }
            }

        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(characterList, status);
    }


    // method for update franchise
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        if(!id.equals(franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise,status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);
    }


}
