package com.example.movieapi.controllers;

import com.example.movieapi.models.Franchise;
import com.example.movieapi.services.FrancheService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

//Method related to services

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchises")

public class FranchiseController {
    //Services initializer
    @Autowired
    FrancheService franserv;

    //Get all franchises
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        return franserv.getAllFranchises();
    }

    //Get a specific franchise
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        return franserv.getFranchise(id);
    }

    //post to add a franchise
    @PostMapping("/add")
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        return franserv.addFranchise(franchise);
    }

    //put to update a franchise
    @PutMapping("/movies/{id}")
    public ResponseEntity<Franchise> updateMovies(@PathVariable Long id, @RequestBody List<Long> moviesId){
        return franserv.updateMovies(id, moviesId);
    }

    // delete a franchise
    @DeleteMapping("/{id}")
    public HttpStatus deleteFranchise(@PathVariable Long id){
        return franserv.deleteFranchise(id);
    }

    //get all movies in a franchise
    @GetMapping("/{id}/movies")
    public ResponseEntity<List<Long>> getMovies(@PathVariable Long id){
        return franserv.getMovies(id);
    }

    // get all characters in a franchise
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Long>> getCharacters(@PathVariable Long id){
        return franserv.getCharacters(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
        return franserv.updateFranchise(id, franchise);
    }

}
