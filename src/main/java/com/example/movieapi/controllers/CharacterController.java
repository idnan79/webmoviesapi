package com.example.movieapi.controllers;

import com.example.movieapi.models.Character;
import com.example.movieapi.services.CharacterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

//all the methods to their services
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
public class CharacterController {
    //Service initializer
    @Autowired
    CharacterService charserv;

    //Get all characters
    @GetMapping()
    public ResponseEntity<List<Character>> getAllCharacters(){
        return charserv.getAllCharacters();
    }

    //Get a specific character
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacter(@PathVariable Long id){
        return charserv.getCharacter(id);
    }

    //add a character
    @PostMapping("/add")
    public ResponseEntity<Character> addCharacter(@RequestBody Character character){
        return charserv.addCharacter(character);
    }

    //update a character
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character){
        return charserv.updateCharacter(id, character);
    }

    //delete a character
    @DeleteMapping("/{id}")
    public HttpStatus deleteCharacter(@PathVariable Long id){
        return charserv.deleteCharacter(id);
    }
}
