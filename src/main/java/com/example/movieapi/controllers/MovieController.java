package com.example.movieapi.controllers;

import com.example.movieapi.models.Movie;
import com.example.movieapi.services.MovieService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

//Methods calls the services
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movies")
public class MovieController {
    //Using Movie service here
    @Autowired
    MovieService movser;
    //Get all movies
    @GetMapping()
    public ResponseEntity<List<Movie>> getAllMovies() {
        return movser.getAllMovies();
    }

    //Get a specific movie
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable Long id) {
        return movser.getMovie(id);
    }

    //Add new movie
    @PostMapping("/add")
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        return movser.addMovie(movie);
    }

    //update characters in a movie
    @PutMapping("/characters/{id}")
    public ResponseEntity<Movie> updateCharacters(@PathVariable Long id, @RequestBody List<Long> characters) {
        return movser.updateCharacters(id, characters);
    }

    //delete a movie
    @DeleteMapping("/{id}")
    public HttpStatus deleteMovie(@PathVariable Long id) {
        return movser.deleteMovie(id);
    }

    //get all characters in a movie
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Long>> getCharacters(@PathVariable Long id) {
        return movser.getCharacters(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie) {
        return movser.updateMovie(id, movie);
    }



}
