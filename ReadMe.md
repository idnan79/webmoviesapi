<h1>MovieCharacterApi</h1> 
![alt text](https://miro.medium.com/max/2000/1*mZyMIiCjDmjNLHFHUc8kbw.png)
Spring Boot application in Java with JDBC and Thymeleaf. The application is about creating a Postgres database with Hibernate and show it with a Web API.
<h2>Tools</h2>
Intellij with java 17-
PostgreSQL with PgAdmin4

<h2>Structure</h2>
- Models
- Repositories
- Representation 


Program description
Program that creates a Web API and database, and seeds it. Then you can apply CRUD on each model.

Install
In terminal: git clone https://gitlab.com/idnan79/webmoviesapi.git
Open the folder in Intellij
How to run
Run by right-clicking WebApiApplication and select Run 'WebApiApplication.main()'

Notes
AppStartRunner:
Two run()-methods: One for production, one for development. Comment out the method you don't want to use, and uncomment the method you want to use.
List of movies:

- Get All
-Get By Id 
- Add New
- Delete
-update 

List of Characters:
List of Franchise:
